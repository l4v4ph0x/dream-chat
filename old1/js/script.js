/* click on contact */
function clickOnContact(id) {
  // unselect all contacts from list
  var contacts = document.querySelectorAll("#contacts ul li");
  contacts.forEach(function(contact) {
    $(contact).removeClass("active");
  });

  var contact = document.querySelector("#contacts ul li#contact_"+id);
  $(contact).addClass("active");

  var elemContent = document.querySelector("#content");
  if (elemContent) {
    page.getConnectedUsers(function(connectedUsers) {
      /* get connected users ids to later check if selected on cantains and so on */
      var connectedUsersIds = [];
      connectedUsers.forEach(function(connectedUser) {
        connectedUsersIds.push(connectedUser["id"]);
      });

      page.getUserById(id, function(user) {

        // set user info modal data
        reloadUserInfoModalById(id);

        page.setActiveChatUserId(user["id"], function() {
          elemContent.innerHTML = "";
          var profileImage = user["profile"] == undefined ? "default/profile.jpg" : page.getUserRelativePath(user)+"upload/"+user["profile"];

          elemContent.innerHTML += `
            <div class="contact-profile">
              <i class="fa fa-cog" style="float: left; line-height:60px; padding-left: 10px; padding-right: 10px; cursor: pointer;" data-toggle="modal" data-target="#selectedUserInfoModal"></i>
              <img class="profile-image_`+user["id"]+`" src="`+profileImage+`" alt="`+user["name"]+` profile image" />
              <p>`+user["name"]+` - <span class="activeUserDescription">`+user["description"]+`</span><span id="errorActiveContantOffline"> | <span style="color: #8b0000" class="fa fa-exclamation selectable"></span></span> </p>
            </div>
            `;

          setTimeout(function() {
            tippy("#errorActiveContantOffline span", {
              content: "This user receives messages once both sides are online, This user is offline!",
            });
          }, 200);


          // hide msg "this user is offline, you cant send messages" if user is already connected
          var contactElem = document.querySelector("#contact_" + user["id"] + " .contact-status");
          if (contactElem) {
            console.log(contactElem.className);
            if (contactElem.className.includes("online") || contactElem.className.includes("away") || contactElem.className.includes("busy")) {
              var errorActiveContantOfflineElem = document.querySelector("#errorActiveContantOffline");
              if (errorActiveContantOffline) {
                errorActiveContantOffline.style.display = "none";
              }
            }
          }

          /* if selected user is our contact list */
          if (connectedUsersIds.includes(user["id"])) {
            /* load write msg */
            elemContent.innerHTML += `
              <div class="messages" id="messages">
                <ul>

                </ul>
              </div>
              <div class="message-input">
                <div class="wrap">
                  <table>
                    <tr><td style="width: 100%;"><input type="text" id="txb-message" placeholder="Write your message ..." onkeypress="sendMessage(event, '`+user["id"]+`')"/></td>
                    <td><label class="submit selectable"><i class="fa fa-picture-o" aria-hidden="true"></i><input class="file" type="file" accept="image/jpeg,image/png" onchange="sendImage(this, '`+user["id"]+`')"></label></td>
                    <td><button class="submit" onclick="sendMessage(13, '`+user["id"]+`')"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></td></tr>
                  </table>
                  <!-- i class="fa fa-paperclip attachment" aria-hidden="true"></i -->

                </div>
              </div>
              `;

            /* set current active user we're chatting */
            page.setActiveChatUserId(user["id"]);

            currentMessagesElem = document.querySelector("#messages");

            // reset automatic scroll down to enabled
            if (currentMessagesElem) {
              // when lastMessageScrollValue is over or qual then
              // disableAutomaticScrolling = false
              lastMessageScrollValue = currentMessagesElem.scrollTop;
            }

            currentChatLinesStart = -DEFAULT_AMOUNT_TO_LOAD_MESSAGES_FIRST;
            alreadyLoadingMessages = true;

            /* load selected user messages */
            page.getChatLinesById(id, currentChatLinesStart, -1, function(lines, isThereMoreMessages) {
              if (isThereMoreMessages) {
                currentMessagesElem.innerHTML = `
                <center id="btn-showMore"><button type="button" class="btn btn-primary btn-sm" onclick="loadMoreChatLines();">Show More</button></center>
                ` + currentMessagesElem.innerHTML;
              }

              // if we got less lines we wanted then we sure reached end,
              // and no need to request more
              if (lines.length < DEFAULT_AMOUNT_TO_LOAD_MESSAGES_FIRST) {
                currentChatLinesStart = 0;
              }

              if (lines.length > 0) {
                // used to check if we want to add ophaned msg to top
                // no need to add orphaned msg to top if its far from msgs we already display
                firstMessageDateFromTop = lines[0]["date"];

                reloadChatLines(lines, 0, id, function() {
                  alreadyLoadingMessages = false;
                });
              }
            });
          } else {
            /* display to add this user to contacts */
            elemContent.innerHTML += `
              <div style="text-align: center; color: #666; margin-top: 10%">
                <span style="cursor: pointer;" onclick="addContact('`+user["id"]+`');">+ add this user to your contact list</span>
              </div>
              `;
          }
        }); // page.setActiveChatUserId
      }); // page.getUserById
    }); // page.getConnectedUsers

  }

  console.log("clicked on contact: " + id);
}

function clearClick() {
  // unselect all contacts from list
  var contacts = document.querySelectorAll("#contacts ul li");
  contacts.forEach(function(contact) {
    $(contact).removeClass("active");
  });

  var elemContent = document.querySelector("#content");
  if (elemContent) {
    elemContent.innerHTML = "";
  }

  currentMessagesElem = undefined;
}

function drawUsers(users) {
  var elemContacts = document.querySelector("#contacts ul");

  /* also available class="contanct active"
   * class="contact-status online"
   * class="contact-status busy"
   */
  if (elemContacts) {
    /* clear contacts before adding new ones */
    elemContacts.innerHTML = "";

    users.forEach(function(user) {
      var profileImage = user["profile"] == undefined ? "default/profile.jpg" : page.getUserRelativePath(user)+"upload/"+user["profile"];

      elemContacts.innerHTML += `
        <li class="contact" id="contact_`+user["id"]+`" onclick="clickOnContact('`+user["id"]+`');">
          <div class="wrap">
            <span class="contact-status"></span>
            <img class="profile-image_`+user["id"]+`" src="`+profileImage+`" alt="`+user["name"]+` profile image" />
            <div class="meta">
              <p class="name">`+user["name"]+`</p>
              <p class="preview"></p>
            </div>
          </div>
        </li>
        `;
    }); // users.forEach

    // dunno why but separating to here works
    for (var i = 0; i < users.length; i++) {
      reloadUserLastMessageById(users[i]["id"]);
    }
  } else {
    console.log(document.querySelector("#contacts"));
    console.error("ERROR: script.js - drawUsers: elemContacts is null");
  }
}

function addContact(id) {
  page.addUserById(id, function() {
    reloadConnectedUsers(function() {
      clickOnContact(id);

      var elemSearch = document.querySelector("#search input");
      if (elemSearch) {
        elemSearch.value = "";
      }
    });
  });
}

function isSearchBoxEmpty() {
  var ret = true;

  var elemSearch = document.querySelector("#search input");
  if (elemSearch) {
    ret = elemSearch.value == "";
  }

   return ret;
}

function userReceivedMessage(dateAsId) {
  var loadingElems = document.getElementsByClassName(dateAsId+"-loading");
  if (loadingElems) {
    loadingElems.remove();
  }
};

function sendMessage(e, id) {
  // if enter is pressed
  if (e == 13 || e.keyCode == 13) {
    var txbMessage = document.querySelector("#txb-message");

    if (txbMessage) {
      var msgToSend = txbMessage.value;

      // disable sending empty messages
      if (msgToSend == undefined || msgToSend.length == 0) {
        return
      }

      addSentMessage(msgToSend, true, function(idToDel) {

        // clear textbox value to write new text
        txbMessage.value = "";

        page.getUserById(id, function(user) {
          if (user["public key"] == undefined) {
            // remove loading message to send if we cant even send it
            var elemToDel = document.querySelector("#"+idToDel);
            if (elemToDel) {
              elemToDel.remove();
            }

            page.showError("this user is missing public key", -1);
          } else {
            // crypt msg with pgp
            page.encryptMessageById(id, msgToSend, function(encrypted) {
              if (encrypted == null) {
                // remove loading message to send if we failed to encrypt it
                var elemToDel = document.querySelector("#"+idToDel);
                if (elemToDel) {
                  elemToDel.remove();
                }

                // also show error notificatoin
                page.showError("Unable to send message to this user, receiver public key incorrect", -1);
              } else {
                var msgDateAsId = Date.now();

                page.addChatLine(id, false, "msg", msgToSend, msgDateAsId, function(isSuccess) {
                  if (!isSuccess) {
                    // remove loading message to send if we failed to save it
                    var elemToDel = document.querySelector("#"+idToDel);
                    if (elemToDel) {
                      elemToDel.remove();
                    }

                    page.showError("Error saving message, please reload page", -1);
                  } else {
                    reloadUserLastMessageById(id);

                    page.peerSend(user["cid"], "msg:" + msgDateAsId + "\n" + encrypted, function(isSuccess) {
                      if (isSuccess == true) {
                        // we have to wait when receiver confirm he got message

                        // add class(dateAsId) to elem for later to remove loading indicator
                        // if user sait he got it
                        var elem = document.querySelector("#"+idToDel);
                        if (elem) {
                          elem.className += " " + msgDateAsId;
                        }
                        var loadingElem = document.querySelector("#"+idToDel+"-loading");
                        if (loadingElem) {
                          loadingElem.className += " " + msgDateAsId + "-loading";
                        }
                      } else {
                        // set it to later receiver scans messages
                        var loadingElem = document.querySelector("#"+idToDel+"-loading");
                        if (loadingElem) {
                          loadingElem.className = msgDateAsId + "-loading fa fa-exclamation selectable";
                        }

                        setTimeout(function() {
                          tippy(".sent .fa-exclamation", {
                            content: "User receives message later, when both sides are online",
                          });
                        }, 200);

                        //page.showError("Error sending message to peer", -1);
                      }
                    }); // page.peerSend
                  } // else of isSuccess
                }); // page.addChatLine
              } // else of if (encrypted == null)
            }); // page.encryptMessageById
          }
        }); // page.getUserById
      }); // addSentMessage
    }
  }
}

function sendImage(e, id) {
  // send multiple images

  for (var i = 0; i < e.files.length; i++) {
    var file = e.files[i];

    var nameSplit = file.name.split(".");
    var extension = nameSplit.last();
    var fileName = makeid(128) + "." + extension;

    page.getUserById(id, function(user) {
      if (user["public key"] == undefined) {
        // remove loading message to send if we cant even send it
        var elemToDel = document.querySelector("#"+idToDel);
        if (elemToDel) {
          elemToDel.remove();
        }

        page.showError("this user is missing public key", -1);
      } else {
        page.cmd("bigfileUploadInit", ["data/users/"+page.siteInfo.auth_address+"/ignore/"+fileName, file.size], (init_res) => {
          var formdata = new FormData();
          formdata.append(file.name, file);

          var req = new XMLHttpRequest();
          req.upload.addEventListener("progress", console.log);
          req.upload.addEventListener("loadend", function() {
            console.log("image loaded");

            page.cmd("fileGet", {"inner_path": "data/users/"+page.siteInfo.auth_address+"/ignore/"+fileName, "required": false, "format": "base64"}, (data) => {
              if (data) {
                //console.log(data);

                addSentImage(fileName, true, function(idToDel) {

                  // include file extension also to data
                  data = extension + "\n" + data;

                  page.encryptMessageById(id, data, function(encrypted) {
                    if (encrypted == null) {
                      // remove loading message to send if we failed to encrypt it
                      var elemToDel = document.querySelector("#"+idToDel);
                      if (elemToDel) {
                        elemToDel.remove();
                      }

                      // also remove failed image from ignore folder
                      page.removeImage(fileName);

                      // also show error notificatoin
                      page.showError("Unable to send Image to this user, receiver public key incorrect", -1);
                    } else {
                      var msgDateAsId = Date.now();

                      page.addChatLine(id, false, "img", fileName, msgDateAsId, function(isSuccess) {
                        if (!isSuccess) {
                          // remove loading message to send if we failed to save it
                          var elemToDel = document.querySelector("#"+idToDel);
                          if (elemToDel) {
                            elemToDel.remove();
                          }

                          // also remove failed image from ignore folder
                          page.removeImage(fileName);

                          page.showError("Error saving message, please reload page", -1);
                        } else {
                          reloadUserLastMessageById(id);

                          page.peerSend(user["cid"], "msg:" + msgDateAsId + "\n" + encrypted, function(isSuccess) {
                            if (isSuccess == true) {
                              // we have to wait when receiver confirm he got message

                              // add class(dateAsId) to elem for later to remove loading indicator
                              // if user sait he got it
                              var elem = document.querySelector("#"+idToDel);
                              if (elem) {
                                elem.className += " " + msgDateAsId;
                              }
                              var loadingElem = document.querySelector("#"+idToDel+"-loading");
                              if (loadingElem) {
                                loadingElem.className += " " + msgDateAsId + "-loading";
                              }
                            } else {
                              // set it to later receiver scans messages
                              var loadingElem = document.querySelector("#"+idToDel+"-loading");
                              if (loadingElem) {
                                loadingElem.className = msgDateAsId + "-loading fa fa-exclamation selectable";
                              }

                              setTimeout(function() {
                                tippy(".sent .fa-exclamation", {
                                  content: "User receives message later, when both sides are online",
                                });
                              }, 200);

                              //page.showError("Error sending message to peer", -1);
                            }
                          }); // page.peerSend
                        } // else of isSuccess
                      }); // page.addChatLine
                      /*
                      page.peerSend(user["cid"], "img:" + encrypted, function(isSuccess) {
                        if (isSuccess == true) {
                          page.addChatLine(id, false, "img", fileName, Date.now(), function() {
                            reloadUserLastMessageById(id);
                          });

                          var elemToDel = document.querySelector("#"+idToDel+"-loading");
                          if (elemToDel) {
                            elemToDel.remove();
                          }

                        } else {
                          // remove loading message to send if we failed to send it
                          var elemToDel = document.querySelector("#"+idToDel);
                          if (elemToDel) {
                            elemToDel.remove();
                          }

                          // also remove failed image from ignor folder
                          page.removeImage(fileName);

                          page.showError("Error sending message to peer", -1);
                        }
                      }); // page.peerSend
                      */
                    } // else of if (encrypted == null)
                  }); // page.encryptMessageById
                }); // addSentImage
              } else {
                console.error("ERROR: sendImage - image data is null");
              }

            });

          }); // req.upload.addEventListener

          req.withCredentials = true;
          req.open("POST", init_res.url);
          req.send(formdata);
        }); // page.cmd("bigfileUploadInit"
      }
    }); // page.getUserById
  }
}

function addSentMessage(msg, loading = false, callback = null, addBackwards = false) {
  var elemMessages = document.querySelector("#messages ul");
  if (elemMessages) {
    msg = wysiwyg(msg);

    page.getUser(function(user) {
      var profileImage = user["profile"] == undefined ? "default/profile.jpg" : page.getUserRelativePath(user)+"upload/"+user["profile"];
      var uid = "l" + makeid(16);
      var toAdd = `
        <li id="`+uid+`" class="sent">
          <img class="profile-image_`+user["id"]+`" src="`+profileImage+`" alt="`+user["name"]+` profile image" />
          <p>`+msg+`</p>
          `+(loading ? `<i id="`+uid+`-loading" class="fa fa-spinner ring"></i>` : ``)+`
        </li>
      `;

      if (addBackwards) {
        elemMessages.innerHTML = toAdd + elemMessages.innerHTML;
      } else {
        elemMessages.innerHTML += toAdd;
      }

      // its here because every msg to need to reload
      $(".pan").pan();

      if (callback) {
        if (loading) {
          callback(uid)
        } else {
          callback();
        }
      }
    });
  }
}

function addSentImage(imgName, loading = false, callback = null, addBackwards = false) {
  var elemMessages = document.querySelector("#messages ul");
  if (elemMessages) {

    page.getUser(function(user) {
      var profileImage = user["profile"] == undefined ? "default/profile.jpg" : page.getUserRelativePath(user)+"upload/"+user["profile"];
      var uid = "i" + makeid(16);
      var toAdd = `
        <li id="`+uid+`" class="sent">
          <img class="profile-image_`+user["id"]+`" src="`+profileImage+`" alt="`+user["name"]+` profile image" />
          <p>
            <a class="pan" data-big="data/users/`+page.siteInfo.auth_address+`/ignore/`+imgName+`" target="_blank" href="data/users/`+page.siteInfo.auth_address+`/ignore/`+imgName+`">
              <img src="data/users/`+page.siteInfo.auth_address+`/ignore/`+imgName+`" class="image-in-chat" style="width: audo; max-width: 200px; height: auto; max-height: 200px"></img>
            </a>
          </p>
          `+(loading ? `<i id="`+uid+`-loading" class="fa fa-spinner ring"></i>` : ``)+`
        </li>
      `;

      if (addBackwards) {
        elemMessages.innerHTML = toAdd + elemMessages.innerHTML;
      } else {
        elemMessages.innerHTML += toAdd;
      }

      $(".pan").pan();

      // callback right away we got element seet down
      if (callback) {
        if (loading) {
          callback(uid)
        } else {
          callback();
        }
      }

      /* resize image to show
      var canvas = document.querySelector("#img_"+uid);
      if (canvas) {
        var img = new Image();
        var context = canvas.getContext('2d');
        var maxW = 200;
        var maxH = 200;

        img.addEventListener("load", function() {
          var iw = img.width;
          var ih = img.height;
          var scale = Math.min((maxW / iw), (maxH / ih));
          var iwScaled = iw * scale;
          var ihScaled = ih * scale;
          canvas.width = iwScaled;
          canvas.height = ihScaled;
          context.drawImage(img, 0, 0, iwScaled, ihScaled);
        });
        img.src = "data:image/png;base64,"+imgBase64;
      }
      */
    });
  }
}

var addingReceivedMessage = false;
function addReceivedMessage(id, msg, callback = null, addBackwards = false, msgDateAsId = null) {
  var waitForAdder = setInterval(function() {
    if (!addingReceivedMessage) {
      addingReceivedMessage = true;
      clearInterval(waitForAdder);

      var elemMessages = document.querySelector("#messages ul");
      if (elemMessages) {
        msg = wysiwyg(msg);

        page.getUserById(id, function(user) {
          var profileImage = user["profile"] == undefined ? "default/profile.jpg" : page.getUserRelativePath(user)+"upload/"+user["profile"];

          // this will be used to substring
          var toAdd = `<li class="replies">
              <img class="profile-image_`+user["id"]+`" src="`+profileImage+`" alt="`+user["name"]+` profile image" />
              <p>`+msg+`</p>
            </li>`;

          if (msgDateAsId != null) {
            var elemos = elemMessages.innerHTML.split("<li");

            // very time taking command, if chats file get in megabytes
            page.getChatLinesById(id, 0, -1, function(lines) {
              var i = lines.length -1;

              // search for lower date then we want to instert and then insert after it
              for (; i >= 0; i--) {
                if (lines[i]["date"] == msgDateAsId) {
                  break;
                }
              }

              if (i >= 0) {
                var insertIndex = elemos.length - (lines.length - i) +1;
                //onsole.log("elemos len: " + elemos.length + " insert i: " + insertIndex);
                elemos.splice(insertIndex, 0, toAdd.substr(3));
                //console.log(elemos);
                elemMessages.innerHTML = elemos.join("<li");
              }

              addingReceivedMessage = false;

            });
          } else {
            if (addBackwards) {
              elemMessages.innerHTML = toAdd + elemMessages.innerHTML;
            } else {
              elemMessages.innerHTML += toAdd;
            }

            addingReceivedMessage = false;
          }

          // its here because every msg to need to reload
          $(".pan").pan();

          if (callback) {
            callback();
          }
        });
      }
    } // if (!addingReceivedMessage) {
  }, 20);
}

var addingReceivedImage = false;
function addReceivedImage(id, imgName, callback = null, addBackwards = false, msgDateAsId = null) {
  var waitForAdder = setInterval(function() {
    if (!addingReceivedImage) {
      addingReceivedImage = true;
      clearInterval(waitForAdder);

      var elemMessages = document.querySelector("#messages ul");
      if (elemMessages) {
        page.getUserById(id, function(user) {
          var profileImage = user["profile"] == undefined ? "default/profile.jpg" : page.getUserRelativePath(user)+"upload/"+user["profile"];

          // this will be used to substring
          var toAdd = `<li class="replies">
              <img class="profile-image_`+user["id"]+`" src="`+profileImage+`" alt="`+user["name"]+` profile image" />
              <p>
                <a class="pan" data-big="data/users/`+page.siteInfo.auth_address+`/ignore/`+imgName+`" target="_blank" href="data/users/`+page.siteInfo.auth_address+`/ignore/`+imgName+`">
                  <img src="data/users/`+page.siteInfo.auth_address+`/ignore/`+imgName+`" class="image-in-chat" style="width: audo; max-width: 200px; height: auto; max-height: 200px"></img>
                </a>
              </p>
            </li>`;

          if (msgDateAsId != null) {
            var elemos = elemMessages.innerHTML.split("<li");

            // very time taking command, if chats file get in megabytes
            page.getChatLinesById(id, 0, -1, function(lines) {
              var i = lines.length -1;

              // search for lower date then we want to instert and then insert after it
              for (; i >= 0; i--) {
                if (lines[i]["date"] == msgDateAsId) {
                  break;
                }
              }

              if (i >= 0) {
                var insertIndex = elemos.length - (lines.length - i) +1;
                //console.log("elemos len: " + elemos.length + " insert i: " + insertIndex);
                elemos.splice(insertIndex, 0, toAdd.substr(3));
                //console.log(elemos);
                elemMessages.innerHTML = elemos.join("<li");
              }

              addingReceivedImage = false;
            });
          } else {
            if (addBackwards) {
              elemMessages.innerHTML = toAdd + elemMessages.innerHTML;

              // also scroll to bottom
              //if (currentMessagesElem) {
              //}
            } else {
              elemMessages.innerHTML += toAdd;
            }

            addingReceivedImage = false;
          }

          $(".pan").pan();

          // callback right away we got element seet down
          if (callback) {
            callback();
          }
        });
      }
    } // if (!addingReceivedImage) {
  }, 20);
}

function setUserStatusById(id, status) {
  var statusElem = document.querySelector("#contact_"+id+" .contact-status");
  if (statusElem) {
    statusElem.className = "contact-status " + status;

    page.getActiveChatUser(function(user) {
      if (user == null) {

      } else {
        if (user["id"] == id) {
          var elem = document.querySelector("#errorActiveContantOffline");
          if (elem) {
            if (status != "offline") {
              if (elem.style.display == undefined || elem.style.display == "") {
                elem.style.display = "none";
              }
            } else {
              if (elem.style.display == "none") {
                elem.style.display = "";
              }
            }
          }
        }
      }
    }); // page.getActiveChatUser
  }
}

/* recursively load lines,
 * load msgs bottom to top
 */
function reloadChatLines(lines, i, id, callback = null) {
  if (i == lines.length) {
    if (callback) {
      callback();
    }
    return;
  }

  var line = lines[lines.length -1 -i];

  if (line["received"] == false) {
    if (line["type"] == undefined || line["type"] == "msg") {
      addSentMessage(line["data"], false, function() {
        reloadChatLines(lines, i +1, id, callback)
      }, true);
    } else if (line["type"] == "img") {
      addSentImage(line["data"], false, function() {
        reloadChatLines(lines, i +1, id, callback);
      }, true);
    }
  } else {
    if (line["type"] == undefined || line["type"] == "msg") {
      addReceivedMessage(id, line["data"], function() {
        reloadChatLines(lines, i +1, id, callback)
      }, true);
    } else if (line["type"] == "img") {
      addReceivedImage(id, line["data"], function() {
        reloadChatLines(lines, i +1, id, callback);
      }, true);
    }
  }
}

function loadMoreChatLines() {
  if (alreadyLoadingMessages) {
    console.warn("WARNING: already loading messages");
  } else {
    alreadyLoadingMessages = true;

    var btnShowMoreElem = document.querySelector("#btn-showMore button");
    if (btnShowMoreElem) {
      btnShowMoreElem.innerHTML += ` <i class="fa fa-spinner ring"></i>`;
    }

    page.getActiveChatUser(function(user) {
      currentChatLinesStart -= DEFAULT_AMOUNT_TO_LOAD_MESSAGES_FIRST +1;

      page.getChatLinesById(user["id"], currentChatLinesStart, currentChatLinesStart + DEFAULT_AMOUNT_TO_LOAD_MESSAGES_FIRST, function(lines, isThereMoreMessages) {

        if (!isThereMoreMessages) {
          var btnShowMoreElem = document.querySelector("#btn-showMore");
          if (btnShowMoreElem) {
            btnShowMoreElem.remove();
          }
        }

        // if we got less lines we wanted then we sure reached end,
        // and no need to request more
        if (lines.length < DEFAULT_AMOUNT_TO_LOAD_MESSAGES_FIRST) {
          currentChatLinesStart = 0;
        }

        if (lines.length > 0) {
          // used to check if we want to add ophaned msg to top
          // no need to add orphaned msg to top if its far from msgs we already display
          firstMessageDateFromTop = lines[0]["date"];

          reloadChatLines(lines, 0, user["id"], function() {
            var btnShowMoreElemLoading = document.querySelector("#btn-showMore button i");
            if (btnShowMoreElemLoading) {
              btnShowMoreElemLoading.remove();
            }

            alreadyLoadingMessages = false;
          });
        }
      });
    }); // page.getActiveChatUser
  }

}

function reloadConnectedUsers(callback = null) {
  // dont load connected user if were searching
  var txbMessage = document.querySelector("#txb-message");

  if (txbMessage == null || txbMessage.value == "") {
    page.getConnectedUsers(function(connectedUsers) {
      console.log("reload connectedUsers:");
      console.log(connectedUsers);

      usersToCheckStatus = connectedUsers;
      drawUsers(connectedUsers);

      if (callback != null) {
        callback();
      }
    }); // page.getConnectedUsers
  }
}

function reloadUserLastMessageById(id) {
  var previewElem = document.querySelector("#contact_"+id+" .preview");

  if (previewElem) {
    page.getUserById(id, function(user) {
      // get and show last chat line
      page.getChatLinesById(id, -1, -1, function(lines) {
        if (lines != undefined) {
          var lastLine = lines.last();

          if (lastLine != undefined) {
            if (lastLine["type"] == undefined || lastLine["type"] == "msg") {
              lastLine["data"] = wysiwyg(lastLine["data"]);
            }
            previewElem.innerHTML = (lastLine["received"] ? "" : "You: ") + (lastLine["type"] == "img" ? "[Image]" : lastLine["data"]);
            //console.log(id + " : " + previewElem.innerHTML);
          }
        }
      }); // page.getChatLinesById
    }); // page.getUserById
  }
}

function reloadUserDescription() {
  var changeDescriptionModalInputElem = document.querySelector("#changeDescriptionModal .modal-body input");
  if (changeDescriptionModalInputElem) {
    page.getUser(function(user) {
      changeDescriptionModalInputElem.value = wysiwyg(user["description"]);
    });
  }
}

function reloadUserInfoModalById(id) {
  page.getUserById(id, function(user) {

    // set user info modal data
    var selectedUserInfoModalTitleElem = document.querySelector("#selectedUserInfoModal .modal-title");
    var selectedUserInfoModalBodyElem = document.querySelector("#selectedUserInfoModal .modal-body");
    if (selectedUserInfoModalTitleElem && selectedUserInfoModalBodyElem) {
      selectedUserInfoModalTitleElem.innerHTML = user["name"];
      selectedUserInfoModalBodyElem.innerHTML = user["description"];
    }
  });
}

function dragOverHandler(ev) {
  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();
}

function dropHandler(ev) {
  console.log('File(s) dropped');

  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();

  var imageFilesEvent = { "files": [] }

  console.log(ev.dataTransfer.items);
  console.log(ev.dataTransfer.files);

  if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to access the file(s)
    for (var i = 0; i < ev.dataTransfer.items.length; i++) {
      // If dropped items aren't files, reject them
      if (ev.dataTransfer.items[i].kind === 'file') {
        var file = ev.dataTransfer.items[i].getAsFile();

        if (file.type.startsWith("image/")) {
          console.log('... file[' + i + '].name = ' + file.name);
          imageFilesEvent.files.push(file);
        }
      }
    }
  } else {
    // Use DataTransfer interface to access the file(s)
    for (var i = 0; i < ev.dataTransfer.files.length; i++) {
      var file = ev.dataTransfer.files[i];

      if (file.type.startsWith("image/")) {
        console.log('... file[' + i + '].name = ' + file.name);
        imageFilesEvent.files.push(file);
      }
    }
  }

  page.getActiveChatUser(function(user) {
    if (user == null) {
      console.error("ERROR: dropHandler - getActiveChatUser user is null");
    } else {
      sendImage(imageFilesEvent, user["id"]);
    }
  });
}
