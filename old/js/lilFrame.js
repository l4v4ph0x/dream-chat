Element.prototype.remove = function() {
  this.parentElement.removeChild(this);
};

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
  for(var i = this.length - 1; i >= 0; i--) {
    if(this[i] && this[i].parentElement) {
      this[i].parentElement.removeChild(this[i]);
    }
  }
};

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

Array.prototype.last = function(){
  return this[this.length - 1];
};

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&|#]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function makeid(length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}

function wysiwyg(text) {
  var ret = escapeHtml(text);

  // replace links
  var link_repl = new RegExp("(http.*?( |$))", "gi");
  ret = ret.replace(link_repl, "<a href='$1' target='_blank'>$1</a>");

  return ret;
}


class ZeroFrameO extends ZeroFrame {

  ZeroFrameO () {
    var self = this;
    self.peer = null;
  }

  main (giveNewId = false, callback = null) {
    console.log("lilFrame.js - main()");
    var self = this;

    self.getSettings(function(data) {
      if (data["dismiss user"] == undefined || data["dismiss user"] == false) {
        self.cmd("siteInfo", {}, (res) => {
          self.siteInfo = res;

          if (self.siteInfo.auth_address && self.siteInfo.cert_user_id) {
            // get new chat id every time we login

            console.log("siteinfo:");
            console.log(self.siteInfo);

            // fix user content.json
            self.fixUserContent(function() {
              if (giveNewId == false) {

                // update site every visit to be sure we are having all ids updated
                self.cmd("siteUpdate", {"address": self.siteInfo.address}, (res) => {
                });

                self.getUser(function(user) {

                  // check if we have proper pgp keys
                  self.getSettings(function(data) {
                    if (data["private key"] == undefined || user["public key"] == undefined) {
                      self.createUserKeyPairs(function(user) {

                      });
                    }
                  });

                  // gather connection ids to remove
                  self.connectionIdsToRemove = []

                  // create peer
                  self.failedToCreatePeerCount = 0;
                  self.overloadFailureToCreatePeer = 0;
                  self.pagePeer = new Peer(user["cid"], {host: "35.204.229.11", port: 9000, path: "/dream-chat"});

                  // just log hoe many failers,
                  // with this we can check did we get failed msg sending
                  // while disconnect happends to resend new
                  self.failCounter = 0;

                  // check for peer to be created and be that way
                  var checkPeerAvailability = function() {
                    if (self.pagePeer == undefined) {
                      self.pagePeer = new Peer(user["cid"], {host: "35.204.229.11", port: 9000, path: "/dream-chat"});
                    } else if (self.pagePeer._disconnected == true || self.pagePeer.disconnected == true) {
                      self.failCounter++;

                      if (self.failedToCreatePeerCount > 10) {
                        self.failedToCreatePeerCount = 0;
                        self.overloadFailureToCreatePeer += 1;

                        if (self.overloadFailureToCreatePeer > 1) {
                          self.failedToCreatePeerCount = -6000;
                          self.overloadFailureToCreatePeer = 0;
                          self.showNotification("peer creating is stand by for 1minute");
                        } else {
                          self.saveUserData("cid", makeid(128), true, function(user) {
                            self.showNotification("Creating new peer");

                            self.pagePeer.destroy();
                            self.pagePeer = new Peer(user["cid"], {host: "35.204.229.11", port: 9000, path: "/dream-chat"});
                            self.onReceiveMessage(self.onReceiveMessageCallback, true);
                          });
                        }
                      } else {
                        self.failedToCreatePeerCount += 1;
                        self.pagePeer.destroy();
                        self.pagePeer = new Peer(user["cid"], {host: "35.204.229.11", port: 9000, path: "/dream-chat"});
                        self.onReceiveMessage(self.onReceiveMessageCallback, true);
                      }
                    } else {
                      self.overloadFailureToCreatePeer = 0;
                      self.failedToCreatePeerCount = 0;

                      // clear connections
                      Object.keys(self.pagePeer.connections).forEach(function(key) {
                        for (var j = 0; j < self.pagePeer.connections[key].length; j++) {
                          if (self.pagePeer.connections[key][j]) {
                            if (self.pagePeer.connections[key][j].pc == null ||
                                self.pagePeer.connections[key][j].pc.iceConnectionState == "failed" ||
                                self.connectionIdsToRemove.includes(self.pagePeer.connections[key][j].id) ||
                                self.pagePeer.connections[key][j].iceConnectionState == "failed" ||
                                self.pagePeer.connections[key][j].peerConnection.iceConnectionState == "closed" ||
                                self.pagePeer.connections[key][j].peerConnection.iceConnectionState == "failed") {

                              // remove id to remove from array, since its a waste to keep
                              var index = self.connectionIdsToRemove.indexOf(self.pagePeer.connections[key][j].id);
                              if (index !== -1) {
                                self.connectionIdsToRemove.splice(index, 1);
                              }

                              if (self.pagePeer.connections[key][j].pc) {
                                self.pagePeer.connections[key][j].pc.close();
                              }
                              if (self.pagePeer.connections[key][j].peerConnection) {
                                self.pagePeer.connections[key][j].peerConnection.close();
                              }

                              self.pagePeer.connections[key][j].close();

                              self.pagePeer.connections[key][j] = null;
                              self.pagePeer.connections[key].splice(j, 1);
                            }
                          }
                        }
                      });

                    }

                    setTimeout(function() {
                      checkPeerAvailability();
                    }, 100);
                  }
                  checkPeerAvailability();

                  // debug open connections
                  /*
                  setInterval(function() {
                    var connectionsCount = 0;

                    Object.keys(self.pagePeer.connections).forEach(function(key) {
                      connectionsCount += self.pagePeer.connections[key].length;
                    });
                    console.log(connectionsCount);

                  }, 10000);
                  setTimeout(function() {
                    var connectionsCount = 0;
                    Object.keys(self.pagePeer.connections).forEach(function(key) {
                      for (var j = 0; j < self.pagePeer.connections[key].length; j++) {
                        if (self.pagePeer.connections[key][j]) {
                          console.log(self.pagePeer.connections[key][j]);
                        }
                      }
                    });
                  }, 4000);
                  */


                  // create new peer every half minute to make sure we always have connection
                  //setInterval(function() {
                  //  var oldPeer = self.pagePeer;
                  //  self.pagePeer = new Peer(user["cid"], {host: "35.204.229.11", port: 9000, path: "/dream-chat"});
                  //  self.onReceiveMessage(self.onReceiveMessageCallback);
                  //  self.pagePeer.destroy();
                  //}, 30000);

                  console.log("peer:");
                  console.log(self.pagePeer);

                  callback(true);
                });
              } else {
                self.saveUserData("cid", makeid(128), false, function(user) {
                  //self.loadHtml("chat", "#body", true);

                  // check for pgp key also, first users also need to have it
                  // also check do we have private key
                  self.getSettings(function(data) {
                    if (data["private key"] == undefined || user["public key"] == undefined) {
                      self.createUserKeyPairs(function(user) {
                        if (callback != null) {
                          callback(user);
                        }
                      });
                    } else {
                      self.userSign(function() {
                        if (callback != null) {
                          callback(user);
                        }
                      }); // self.userSign
                    }
                  });

                }); // self.saveUserData
              }
            }); // self.fixUserContent
          } else {
            callback(false);
          }
        });
      } else {
        callback(false);
      }
    });

  }

  onOpenWebsocket () {
    // sucessfully connected
  }

  onRequest (cmd, message) {
    var self = this;

    if (cmd == "setSiteInfo") {
      // if login is success
      if (message.params.auth_address && message.params.cert_user_id) {
        //self.loadHtml("chat", "#body", true);

        self.getSettings(function(data) {
          data["dismiss user"] = false;

          self.cmd("userSetSettings", [data], (res) => {
            // gets way 2 much called
            //console.log("lilFrame.js - onRequest: userSetSettings " + res);

            if (self.loginCallback != undefined) {
              self.loginCallback();
            }
          });
        });
      } else {

      }

      // Save site info data to allow access it later
      self.siteInfo = message.params;
    }
  }

  selectUser (callback) {
    var self = this;
    self.loginCallback = callback;

    self.cmd("certSelect", {accepted_domains: ["zeroid.bit"]});
  }

  fixUserContent (callback) {
    var self = this;

    self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/content.json", "required": false}, (data) => {
      var content = JSON.parse(data);

      if (content == null) {
        self.getUser(function(user) {
          self.fixUserContent(callback);
        });
      } else {
        var changed = false;

        if (content["optional"] == undefined) {
          content["optional"] = "(upload/.*)";
          changed = true;
        }

        if (content["ignore"] == undefined) {
          content["ignore"] = "(ignore/.*)";
          changed = true;
        }

        if (changed) {
          var json_raw = unescape(encodeURIComponent(JSON.stringify(content)));
          self.cmd("fileWrite", ["data/users/"+self.siteInfo.auth_address+"/content.json", btoa(json_raw)], (res) => {
            if (res == "ok") {

            } else {
              console.error("ERROR: fixUserContent - fileWrite: failed");
            }

            callback();
          });
        } else {
          callback();
        }
      }

    }); // self.cmd("fileGet"
  }

  dismissUser(callback) {
    var self = this;

    self.getSettings(function(data) {
      data["dismiss user"] = true;

      self.cmd("userSetSettings", [data], (res) => {
        console.log("lilFrame.js - dismissUser: userSetSettings " + res);
        callback();
      });
    });
  }

  getSettings (callback) {
    var self = this;

    self.cmd("userGetSettings", [], (res) => {

      // check some none keys in dict
      if (res["connected users"] == undefined) {
        res["connected users"] = [];
      }

      callback(res);
    });
  }

  getUser (callback) {
    console.log("lilFrame.js - getUser(callback)");

    var self = this;

    if (self.currentUser == null) {
      if (self.siteInfo && self.siteInfo.auth_address && self.siteInfo.cert_user_id) {
        self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/data.json", "required": false}, (data) => {
          if (data) {
            var user = JSON.parse(data);
            user["id"] = self.siteInfo.auth_address;
            user["name"] = self.siteInfo.cert_user_id.split("@")[0];
            if (user["description"] == undefined) {
              user["description"] = self.DEFAULT_USER_DECRIPTION;
            }

            self.currentUser = user;
            callback(user);
          } else {
            // user does not exists yet
            self.saveUserData("cid", makeid(128), true, function(user) {
              self.currentUser = user;
              callback(user);
            });
          }

        });
      } else {
        callback(null);
      }
    } else {
      callback(self.currentUser);
    }
  }

  userSign(callback = null) {
    var self = this;
    var contentInnerPath = "data/users/" + self.siteInfo.auth_address + "/content.json";

    // Sign the changed file in our user's directory
    self.cmd("siteSign", {"inner_path": contentInnerPath}, (res) => {

      // clear current user from buffer to read it again
      self.currentUser = null;

      // Publish to other users
      self.cmd("sitePublish", {"inner_path": contentInnerPath, "sign": false}, function(res) {

      });

      if (callback != null) {
        callback();
      }
    });
  }

  getUserById (id, callback) {
    // goin to spam too much
    //console.log("lilFrame.js - getUserById("+id+", callback)");

    var self = this;

    self.cmd("fileGet", {"inner_path": "data/users/"+id+"/data.json", "required": false}, (data) => {
      if (data) {
        var user = JSON.parse(data);
        user["id"] = id;

        self.cmd("fileQuery", ["data/users/"+id+"/content.json"], (content) => {
          user["name"] = content[0]["cert_user_id"].split("@")[0];
          if (user["description"] == undefined) {
            user["description"] = self.DEFAULT_USER_DECRIPTION;
          }

          // use this pgpReWrites to apply public key fix
          if (self.pgpReWrites[user["cid"]] != undefined) {
            user["public key"] = self.pgpReWrites[user["cid"]];
          }

          callback(user);
        });
      } else {
        console.error("ERROR: lilFrame.js - getUserById("+id+") user data doest not exists");
        callback(null);
      }
    });
  }

  getUserByCId (cid, callback) {
    var self = this;

    self.getConnectedUsers(function(users) {
      var found = false;

      // first search from contacts or connect users
      for (var i = 0; i < users.length; i++) {
        if (users[i]["cid"] == cid) {
          callback(users[i]);
          found = true;
          break;
        }
      }

      // if we havent found in contacts then search from all users
      if (!found) {
        self.cmd("fileQuery", ["data/users/*/data.json"], (files) => {
          for (var i = 0; i < files.length; i++) {
            if (files[i]["cid"] == cid) {
              self.getUserById(files[i]["inner_path"], function(user) {
                callback(user);
              });
            }
          }
        }); // self.cmd("fileQuery"
      }
    });
  }

  saveUserData(key, value, publish = true, callback = null) {
    var self = this;
    var user = {};
    var dataInnerPath = "data/users/" + self.siteInfo.auth_address + "/data.json";
    var contentInnerPath = "data/users/" + self.siteInfo.auth_address + "/content.json";

    self.cmd("fileGet", {"inner_path": dataInnerPath, "required": false}, (data) => {
      if (data) {
        user = JSON.parse(data);
      }

      user[key] = value;

      var jsonRaw = unescape(encodeURIComponent(JSON.stringify(user, undefined, '\t')));

      // Write file to disk
      self.cmd("fileWrite", [dataInnerPath, btoa(jsonRaw)], (res) => {
        if (res == "ok") {
          // null current user to force read it again
          self.currentUser = null;

          if (publish) {
            self.userSign(function() {
              if (callback != null) {
                self.getUser(function(user) {
                  callback(user);
                }); // self.getUser
              }
            }); // self.userSign
          } else {
            if (callback != null) {
              self.getUser(function(user) {
                callback(user);
              }); // self.getUser
            }
          }
        } else {
          self.cmd("wrapperNotification", ["error", "File write error: #{res}"]);

          if (callback != null) {
            callback(null);
          }
        }
      });
    });

  }

  getUserRelativePath (user) {
    return  "data/users/" + user["id"] + "/";
  }

  searchUsers (searchX, callback) {
    var self = this;

    if (searchX == undefined || searchX == null || searchX == "") {
      callback([]);
      return;
    }

    self.cmd("fileList", ["data/users/"], (files) => {
      var ret = [];

      self.getUser(function(user) {
        var ids = files.filter(function(el) {
          return el.startsWith("1") && el.endsWith("content.json");
        });

        for (var i = 0; i < ids.length; i++) {
          setTimeout(function(j) {
            var id = ids[j].split("/")[0];

            // mae sure we dont search our own user
            if (id != user["id"]) {
              self.getUserById(id, function(userById) {
                if (userById == null) {
                  console.error("ERROR: searchUsers - userById is null");
                } else {
                  if (userById.name.toLowerCase().includes(searchX.toLowerCase())) {
                    ret.push(userById);
                  }
                }

                if (j == ids.length -1) {
                  callback(ret);
                }
              });
            }

          }, 0, i);
        }
      });
    });
  }

  getConnectedUsers (callback) {
    var self = this;

    self.getSettings(function(data) {
      var users = data["connected users"];
      var ret = [];

      if (users == undefined) {
        callback(ret);
        return;
      }

      // fix added double users somehow
      var uniqueUsers = [];

      for (var i = 0; i < users.length; i++) {
        var found = false;

        for (var j = 0; j < uniqueUsers.length; j++) {
          if (users[i]["id"] == uniqueUsers[j]["id"]) {
            found = true;
            break;
          }
        }

        if (!found) {
          uniqueUsers.push(users[i]);
        }
      }

      users = uniqueUsers;
      if (users.length == 0) {
        callback(ret);
      }

      users = users.sort(function(a, b) {
        if (a["last edit"] == undefined) a["last edit"] = 0;
        if (b["last edit"] == undefined) b["last edit"] = 0;

        if (a["last edit"] > b["last edit"]) return -1;
        if (a["last edit"] < b["last edit"]) return 1;

        return 0;
      });

      for (var i = 0; i < users.length; i++) {
        setTimeout(function(j) {
          var id = users[j]["id"];

          self.getUserById(id, function(user) {
            if (user == null) {
              console.error("ERROR: searchUsers - user is null");
            } else {
              ret.push(user);
            }

            if (j == users.length -1) {
              callback(ret);
            }
          });
        }, 0, i);
      }
    });

  }

  addUserById (id, callback = null) {
    var self = this;

    self.getSettings(function(data) {
      // check if id is already in connected users
      var alreadyConnected = false;
      for (var i = 0; i < data["connected users"]; i++) {
        if (data["connected users"][i]["id"] == id) {
          alreadyConnected = true;
          break;
        }
      }

      if (!alreadyConnected) {
        data["connected users"].push({ "id": id, "last edit": Date.now(), "messages": [] });

        self.cmd("userSetSettings", [data], (res) => {
          console.log("lilFrame.js - addUser: userSetSettings " + res);

          // let know wer have added
          if (callback != null) {
            callback(true);
          }
        });
      } else {
        callback(false);
      }
    });
  }

  removeUserById (id, callback = null) {
    var self = this;

    self.getSettings(function(data) {
      var connectedUsers = [];

      for (var i = 0; i < data["connected users"].length; i++) {
        if (data["connected users"][i]["id"] != id) {
          connectedUsers.push(data["connected users"][i]);
        }
      }

      data["connected users"] = connectedUsers;

      // save settings, after we have addded chat line
      self.cmd("userSetSettings", [data], (res) => {
        console.log("lilFrame.js - removeUserById: userSetSettings " + res);

        self.setActiveChatUserId("", function() {

        });

        // let know we r done with clearing
        if (callback != null) {
          callback();
        }
      });
    });
  }

  /* id of user we r talkin to and did we receive msg or we sent and finally the msg */
  addChatLine (userId, receivedMsg, dataType, msg, date, callback = null) {
    var self = this;

    self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/ignore/"+userId, "required": false}, (data) => {
      var jdata = null;

      if (data) {
        jdata = JSON.parse(data);
      } else {
        jdata = { "messages": [] }
      }

      // check if this msg by date already exists and not add it
      var messageAlreadyExists = false;
      for (var i = 0; i < jdata["messages"].length; i++) {
        if (jdata["messages"][i]["date"] == date) {
          messageAlreadyExists = true;
          break;
        }
      }

      if (!messageAlreadyExists) {
        jdata["messages"].push({ "type": dataType, "data": msg, "received": receivedMsg, "date": date });
        var jraw = unescape(encodeURIComponent(JSON.stringify(jdata, undefined, '\t')));

        self.cmd("fileWrite", ["data/users/"+self.siteInfo.auth_address+"/ignore/"+userId, btoa(jraw)], (res) => {
          if (res == "ok") {
            if (callback != null) {
              callback(true);
            }
          } else {
            console.error("ERROR: addChatLine - fileWrite failed : ignore/" + userId);
            if (callback != null) {
              callback(false);
            }
          }
        });
      } else {
        console.error("ERROR: addChatLine - message already exists by date: " + date);

        if (callback != null) {
          callback(false);
        }
      }

    });
  }

  getRawChatLinesById (userId, callback) {
    var self = this;

    self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/ignore/"+userId, "required": false}, (data) => {
      if (data) {
        var jdata = JSON.parse(data);

        callback(jdata["messages"]);
      } else {
        callback([]);
      }
    });
  }

  /*
   * callback returns lines and is there more lines as boolean
   */
  getChatLinesById (userId, start, end, callback) {
    var self = this;

    self.getRawChatLinesById(userId, function(lines) {
      lines = lines.sort(function(a, b) {
        if (a["date"] < b["date"]) return -1;
        if (a["date"] > b["date"]) return 1;
        return 0;
      });

      var isThereMoreinesAhead = false;

      if (end < 0) {
        end += lines.length +1;
      }

      if (start < 0) {
        start += lines.length;
        isThereMoreinesAhead = start > 0;
      } else {
        isThereMoreinesAhead = end < lines.length
      }

      if (start < 0) {
        start = 0;
      }

      callback(lines.slice(start, end), isThereMoreinesAhead);
    });
  }

  clearChatLinesById (userId, callback) {
    var self = this;

    self.getRawChatLinesById(userId, function(lines) {
      // check for images and delete them
      lines.forEach(function(line) {
        if (line["type"] == "img") {
          self.removeImage(line["data"]);
        }
      });

      var jdata = { "cleared": Date.now(), "messages": [] }
      var jraw = unescape(encodeURIComponent(JSON.stringify(jdata, undefined, '\t')));

      self.cmd("fileWrite", ["data/users/"+self.siteInfo.auth_address+"/ignore/"+userId, btoa(jraw)], (res) => {
        if (callback != null) {
          callback();
        }

        if (res == "ok") {

        } else {
          console.error("ERROR: setActiveChatUserId - fileWrite failed : ignore/" + userId);
        }
      });
    });
  }

  getClearedDateById (userId, callback) {
    var self = this;

    self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/ignore/"+userId, "required": false}, (data) => {
      if (data) {
        var jdata = JSON.parse(data);

        callback(jdata["cleared"] == undefined ? 0 : jdata["cleared"]);
      } else {
        callback(0);
      }
    });
  }

  setActiveChatUserId (id, callback = null) {
    var self = this;

    self.getSettings(function(data) {
      data["active chat user id"] = id;

      self.cmd("userSetSettings", [data], (res) => {
        console.log("lilFrame.js - setActiveChatUserId: userSetSettings " + res);

        // let know wer have added
        if (callback != null) {
          callback();
        }
      });

    });
  }

  getActiveChatUser (callback) {
    var self = this;

    self.getSettings(function(data) {
      var id = data["active chat user id"];

      if (id == undefined || id == null || id == "") {
        callback(null);
      } else {
        self.getUserById(id, function(user) {
          callback(user);
        });
      }

    });

  }

  setUserStatus (status, callback) {
    var self = this;

    self.getSettings(function(data) {
      data["status"] = status;
      self.cmd("userSetSettings", [data], (res) => {
        console.log("lilFrame.js - setUserStatus: userSetSettings " + res);

        // let know we'r done with dat
        if (callback != null) {
          callback();
        }
      });
    });
  }

  getUserStatus (callback) {
    var self = this;

    self.getSettings(function(data) {
      if (data["status"] == undefined) {
        callback("online");
      } else {
        callback(data["status"]);
      }
    });
  }

  getUserStatusById (id, callback = null) {
    var self = this;

    self.getUserById(id, function(user) {
      if (user == null) {
        console.error("ERROR: lilframe.js - searchUsers: user is null");
      } else {
        self.peerSend(user["cid"], "status?", function(isSuccess) {
          // request is sent and receive handler is gonna filter out
          // at first all users are offline

          if (callback != null) {
            callback(isSuccess);
          }
        });
      }
    });
  }

  createUserKeyPairs (calblack = null) {
    var self = this;

    self.getUser(function(user) {
      var options = {
        userIds: [{ name: user["name"], id: user["id"] }],
        curve: "ed25519",
        passphrase: self.siteInfo.auth_key
      };

      openpgp.generateKey(options).then(function(key) {
        var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
        var pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
        //var revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ...

        self.saveUserData("public key", pubkey, true, function(user) {
          self.setUserPrivateKey(privkey, function() {
            self.showNotification("Created new PGP keys");

            // let know by callback we r dont with dat
            if (calblack != null) {
              calblack(user);
            }
          });
        }); // self.saveUserData
      });//  openpgp.generateKey
    }); // self.getUser
  }

  setUserPrivateKey (privateKey, callback = null) {
    var self = this;

    self.getSettings(function(data) {
      data["private key"] = privateKey;
      self.cmd("userSetSettings", [data], (res) => {
        console.log("lilFrame.js - setUserPrivateKey: userSetSettings " + res);

        // let know we'r done with dat
        if (callback != null) {
          callback();
        }
      }); //self.cmd("userSetSettings"
    }); // self.getSettings
  }

  getUserPricateKey (callback) {
    var self = this;

    self.getSettings(function(data) {
      callback(data["private key"]);
    });
  }

  encryptMessageById (id, toEncrypt, callback) {
    var self = this;

    self.getUserById(id, function(user) {
      openpgp.key.readArmored(user["public key"]).then(armored => {
        if (armored.keys.length == 0) {
          console.error("ERROR: encryptMessageById - error occur while encrypting");
          console.log("lilFrame.js - encryptMessageById: asking new public key to encrypt");

          self.askNewPublicKeyById(id, function(publicKey) {
            // apply public key fix
            // we already have it by cid
            //self.pgpReWrites[id] = publicKey;

            // full copy of above to not call this function again and get into loop
            self.getUserById(id, function(nUser) {
              openpgp.key.readArmored(nUser["public key"]).then(armored => {
                var options = {
                  message: openpgp.message.fromText(toEncrypt),
                  publicKeys: armored.keys
                }

                openpgp.encrypt(options).then(ciphertext => {
                  var encrypted = ciphertext.data;
                  callback(encrypted);
                }); // openpgp.encrypt
              }); // openpgp.key.readArmored
            });

          });
        } else {
          var options = {
            message: openpgp.message.fromText(toEncrypt),
            publicKeys: armored.keys
          }

          openpgp.encrypt(options).then(ciphertext => {
            var encrypted = ciphertext.data;
            callback(encrypted);
          }); // openpgp.encrypt
        }
      }); // openpgp.key.readArmored
    }); // self.getUser

  }

  decryptMessage (encryptedMessage, callback) {
    var self = this;

    self.getSettings(function(data) {
      openpgp.key.readArmored(data["private key"]).then(privArmored => {
        var privKeyObj = privArmored.keys[0];
        if (privKeyObj == undefined) {
          self.showError("please recreate your pgp key from More Options", -1);
        } else {
          privKeyObj.decrypt(self.siteInfo.auth_key).then(function() {
            openpgp.message.readArmored(encryptedMessage).then(msg => {
              var options = {
                  message: msg,
                  privateKeys: [privKeyObj]
              }

              openpgp.decrypt(options).then(plaintext => {
                //var text = wysiwyg(plaintext.data);
                //callback(text);
                callback(plaintext.data);
              }); // openpgp.decrypt
            }); // penpgp.message.readArmored(encryptedMessage
          }); // privKeyObj.decrypt
        }
      }); // openpgp.key.readArmored(localUser["private key"]
    }); // self.getUser
  }

  askNewPublicKeyById (userId, callback) {
    var self = this;

    page.getUserById(userId, function(user) {

      var intervo = setInterval(function() {
        if (self.pgpReWrites[user["cid"]]) {
          clearInterval(intervo);
          callback(self.pgpReWrites[user["cid"]]);
        }
      }, 100);

      self.peerSend(user["cid"], "publicKey?", function(isSuccess) {
        // if we failed to send ask key then return nothing
        if (!isSuccess) {
          callback(null);
        }
      });
    });
  }

  confirmReceivedMessageByCId (cid, dateAsId, callback = null) {
    var self = this;

    if (cid == undefined || cid == null || cid == "") {
      console.error("ERROR: confirmReceivedMessage - cid is undefined, null or empty");
    } else {
      self.peerSend(cid, "msgConfirm:" + dateAsId, function(isSuccess) {
        // callback, was this success
        if (callback != null) {
          callback(isSuccess);
        }
      });
    }
  }

  /* askSentMessagesIdsByCId: ask to get sent message to me
   * fromid: -1 = end, 0 = start, any other value means dateasid
   * length: -2 = 2 back from id, 2 = 2 forward from id
   */
  askSentMessagesIdsByCId (cid, fromid, length, callback = null) {
    var self = this;

    if (cid == undefined || cid == null || cid == "") {
      console.error("ERROR: askSentMessagesIdsByCId - cid is undefined, null or empty");
      if (callback != null) {
        callback(false);
      }
    } else {
      self.peerSend(cid, "sentMsgs?" + fromid + "," + length, function(isSuccess) {
        // callback, was this success
        if (callback != null) {
          callback(isSuccess);
        }
      });
    }
  }

  /* getSentMessageIds: get sent messages based on fromid and length
   * fromid: -1 = end, 0 = start, any other value means dateasid
   * length: -2 = 2 back from id, 2 = 2 forward from id
   */
  getSentMessageIdsById (id, fromid, length, callback) {
    var self = this;

    self.getChatLinesById(id, 0, -1, function(lines) {
      var ret = [];

      if (fromid == 0) {
        var i = 0;
        var j = 0;
        for (; i < lines.length && j < length; i++) {
          if (lines[i]["received"] == false) {
            ret.push(lines[i]["date"]);
            j++;
          }
        }
      } else if (fromid == -1) {
        length = Math.abs(length);

        var i = lines.length -1;
        var j = 0;
        for (; i >= 0 && j < length; i--) {
          if (lines[i]["received"] == false) {
            ret.push(lines[i]["date"]);
            j++;
          }
        }
      } else {
        var i = lines.length -1;

        for (; i >= 0; i--) {
          if (lines[i]["date"] == fromid) {
            break;
          }
        }

        i--;

        // i gets -1 if we did not find even in index 0, cause for does -1 at end
        if (i > -1) {
          var j = 0;
          var indexAdder = length > 0 ? 1 : -1
          length = Math.abs(length);

          for (; i >= 0 && i < lines.length && j < length; i += indexAdder) {
            if (lines[i]["received"] == false) {
              ret.push(lines[i]["date"]);
              j++;
            }
          }
        }

      }

      callback(ret);
    });
  }

  /* cmp messages
   *
   */
  verifyMessagesAndAskMissingById (id, msgIds, callback = null) {
    var self = this;

    self.getUserById(id, function(userById) {
      if (userById == null) {
        console.error("ERROR: verifyMessagesAndAskMissingById - no user to id: " + id);
      } else {
        self.getRawChatLinesById(id, function(lines) {
          self.getClearedDateById(id, function(clearedDate) {
            msgIds.forEach(function(msgId) {
              if (msgId > clearedDate) {
                var i = 0;
                for (; i < lines.length; i++) {
                  if (lines[i]["date"] == msgId) {
                    break;
                  }
                }

                // if message to this date isnt found then ask to resend it
                if (i == lines.length) {
                  self.askMessageToResendByCId(userById["cid"], msgId, function(isSuccess) {
                    // Nothing to do here :P
                  });
                }
              }

            });

            // place to let user know we have done important stuff
            if (callback != null) {
              callback();
            }
          }); // self.getClearedDateById
        }); // self.getRawChatLinesById
      }
    });
  }

  /* asks sender to resend message by dateAsId
   * msgId: message id is also a date message sent out
   */
  askMessageToResendByCId (cid, msgId, callback = null) {
    var self = this;

    if (cid == undefined || cid == null || cid == "") {
      console.error("ERROR: askMessageToResentByCId - cid is undefined, null or empty");
    } else {
      self.peerSend(cid, "resendMsg:" + msgId, function(isSuccess) {
        // callback, was this success
        if (callback != null) {
          callback(isSuccess);
        }
      });
    }
  }

  /* get message by its date(also date is msg id)
   * userId: user who to who we have sent message
   * dateAsId: message date and also id
   */
  getMessageById (userId, dateAsId, callback) {
    var self = this;

    self.getRawChatLinesById(userId, function(lines) {
      var i = 0;
      for (; i < lines.length; i++) {
        if (lines[i]["date"] == dateAsId) {
          break;
        }
      }

      if (i == lines.length) {
        callback(null);
      } else {
        callback(lines[i]);
      }
    });
  }

  /*
   * userId: user who to who we have sent message
   * dateAsId: message date and also id
   */
  resendMessageById (userId, dateAsId, callback) {
    var self = this;

    self.getUserById(userId, function(userById) {
      if (userById == null) {
        console.error("ERROR: resendMessageByDateAsId - no user by id: " + userId);
        callback(false);
      } else {
        self.getMessageById(userId, dateAsId, function(msg) {
          if (msg == null) {
            console.error("ERROR: resendMessageByDateAsId - no message by dateAsId: " + dateAsId);
            callback(false);
          } else {
            var dataToEncrypt = null;

            if (msg["type"] == "img") {
              self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/ignore/"+msg["data"], "required": false, "format": "base64"}, (data) => {
                if (data) {
                  var extension = msg["data"].split(".").last();
                  dataToEncrypt = extension + "\n" + data;
                }
              });
            } else {
              dataToEncrypt = msg["data"];
            }

            var intervo = setInterval(function() {
              if (dataToEncrypt != null) {
                clearInterval(intervo);

                self.encryptMessageById(userId, dataToEncrypt, function(encrypted) {
                  if (encrypted == null) {
                    // Ignore error, cause its impossible anyway to send
                    callback(false);
                  } else {
                    page.peerSend(userById["cid"], msg["type"] + ":" + msg["date"] + "\n" + encrypted, function(isSuccess) {
                      callback(isSuccess);
                    });
                  }
                });
              }
            }, 20);

          }
        });
      }
    });
  }

  /* add image to ignore folder and return its id(not path)
   * imgBase64: base64 image string
   */
  addImage (imgBase64, callback) {
    var self = this;
    var extension = imgBase64.substr(0, imgBase64.indexOf("\n"));
    imgBase64 = imgBase64.substr(extension.length +1);
    var uid = makeid(128) + "." + extension;

    // check if we have set "own": true in siteinfo
    self.cmd("fileWrite", ["data/users/"+self.siteInfo.auth_address+"/ignore/"+uid, imgBase64], (res) => {
      if (res == "ok") {
        callback(uid);
      } else {
        console.error("ERROR: addImage - fileWrite failed for image: ignore/" + uid);
      }
    });
    //self.cmd()
  }

  /* remove image based its id(name)
   * imgName: image name in ignore folder
   */
  removeImage (imgName, callback = null) {
    var self = this;

    self.cmd("fileDelete", ["data/users/"+self.siteInfo.auth_address+"/ignore/"+imgName], (res) => {
      if (res == "ok") {
        if (callback) {
          callback();
        }
      } else {
        console.error("ERROR: removeImage - fileDelete failed for image: ignore/" + imgName);
      }
    });
  }

  onReceiveMessage (callback, isMain = false) {
    var self = this;

    if (callback == null || callback == undefined) {
      console.error('ERROR: onReceiveMessage - callback is null or undefined');
    } else {
      if (isMain) {
        self.onReceiveMessageCallback = callback;
      }

      // receive msg
      self.pagePeer.on("connection", function(conn) {
        //self.addConnection(conn);

        //console.log(conn);
        conn.on("data", function(data) {
          //console.log("onReceiveMessage: " + data);
          callback({"peer": conn.peer, "data": data});

          setTimeout(function() {
            if (conn) {
              conn.close();
              conn = null;
            }
          }, 100);
        });

        conn.on("close", function() {
          if (conn) {
            conn.close();
            conn = null;
          }
        });

        conn.on("disconnected", function() {
          if (conn) {
            conn.close();
            conn = null;
          }
        });

        conn.on("error", function() {
          if (conn) {
            conn.close();
            conn = null;
          }
        });
      });
    }
  }

  peerSend (cid, msg, callback = null, triesLeft = 2) {
    var self = this;
    triesLeft--;

    if (triesLeft <= 0) {
      if (callback) {
        callback(false);
      }
    } else {
      //console.log("msg: " + msg);

      var conn = self.pagePeer.connect(cid);
      if (conn == undefined) {
        //self.pagePeer.close();
        //console.log(self.pagePeer);
        //console.log(conn);

        if (msg != "status?" && !msg.startsWith("sentMsgs?")) {
          // occurous way too much
          //self.showNotification("resending  message while peer got fuked up", 5000);

          setTimeout(function() {
            self.peerSend(cid, msg, callback);
          }, 200);
        }

        return;
      }

      // for debugging start of conn
      //console.log("---------- start of conn");
      //console.log(conn);

      conn.on("close", function() {
        if (conn) {
          conn.close();
          conn = null;
        }
      });

      conn.on("disconnected", function() {
        if (conn) {
          conn.close();
          conn = null;
        }
      });

      conn.on("error", function() {
        if (conn) {
          conn.close();
          conn = null;
        }
      });


      // this timeout weill get be cleared out when connection gets open and msg sent
      var checkTimeout = setTimeout(function() {
        if (conn) {
          self.connectionIdsToRemove.push(conn.id);
        }

        setTimeout(function() {
          self.peerSend(cid, msg, callback, triesLeft -1);
        }, 400);
      }, 8000);

      var oldFailCounts = self.failCounter;
      var checkErrors = setTimeout(function() {
        if (oldFailCounts < self.failCounter) {
          // make sure to remove orphaned connection
          if (conn) {
            self.connectionIdsToRemove.push(conn.id);
          }

          // status ask is going again, so no need to over flow channel
          if (msg != "status?" && !msg.startsWith("sentMsgs?")) {
            // we r gonna send new so no timout now
            clearTimeout(checkTimeout);

            // we r fuked, have to send msg again
            console.error("ERROR: peerSend - connection cut while sending msg, resending in 400ms");
            setTimeout(function() {
              self.peerSend(cid, msg, callback);
            }, 400);

          }
        }
      }, 200);

      conn.on("open", function() {
        //self.addConnection(conn);
        conn.send(msg);

        // reset fail counter, not to over load
        self.failCounter = 0;

        clearTimeout(checkTimeout);
        clearTimeout(checkErrors);

        if (callback != null) {
          callback(true);
        }
      });
    }
  }

  showNotification (msg, time = 5000) {
    var self = this;

    if (time == -1) {
      self.cmd("wrapperNotification", ["info", msg], (res) => {

      });
    } else {
      self.cmd("wrapperNotification", ["info", msg, time], (res) => {

      });
    }
  }

  showError (msg, time = 5000) {
    var self = this;

    if (time == -1) {
      self.cmd("wrapperNotification", ["error", msg], (res) => {

      });
    } else {
      self.cmd("wrapperNotification", ["error", msg, time], (res) => {

      });
    }
  }
}

page = new ZeroFrameO();
page.currentUser = null;
page.DEFAULT_USER_DECRIPTION = "Random Dream Chat User";

// users who have problem decrypting or encrypting with pgpkey then
// new has to be askeb with peerjs
page.pgpReWrites = {};

/*
$(window).on('beforeunload', function() {
  if (page.pagePeer != undefined) {
    page.pagePeer.destroy();
  }
});
*/
