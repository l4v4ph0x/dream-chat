
class Login {
  constructor() {

    lilframe.getLanguageFileAsJsonObject("langs/en-login.json", (jo) => {
      lavatube.render("templates/login.html", {lang: jo});
    });


    lavatube.onMethod({
      onLogin: () => {
        lilframe.selectUser();
      }
    });

    lilframe.onLogin(() => {
      window.location.search = "";
    });
  }
}