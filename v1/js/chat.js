
class Chat {
  contacts;
  lang;
  currentUser;

  moduleSettings;

  constructor() {
    let self = this;

    lilframe.getLanguageFileAsJsonObject("langs/en-main.json", (lang) => {

      self.lang = lang;

      lilframe.getCurrentUser((user) => {
        self.currentUser = user;

        lilframe.getCurrentUserContacts((contacts) => {

          self.contacts = contacts;

          lavatube.render("templates/main.html", {
            connectedContacts: contacts,
            username: user['name'],
            lang: lang
          });

          chatDreamWorks.tellMyAddress(user['id']);

        }); // lilframe.getCurrentUserContacts
      }); // lilframe.getCurrentUser
    }); // lilframe.getLanguageFileAsJsonObject



    lavatube.onMethod({
      onSettings: () => {
        self.moduleSettings = new Settings(self.lang, self.currentUser);
      },
      onContact: (contactId) => {
        //lavaTubeGlobal.setHref("?chat="+contactId);

        for (let contact of self.contacts) {
          if (contact.id === contactId) {
            if (contact.accepted !== true) {

              if (contact.publicKey === undefined || contact.publicKey === "") {
                chatDreamWorks.getPublicKey(self.currentUser['id'], contactId, self.currentUser['publicKey'], () => {

                });

                lavatube.renderArea("right-content", "templates/main-waiting-for-key.html", {
                  lang: self.lang
                });
              } else {
                lavatube.renderArea("right-content", "templates/main-accept-user.html", {
                  lang: self.lang,
                  contactId: contact.id
                });
              }
            } else {
              lavatube.renderArea("right-content", "templates/main-chat.html", {
                lang: self.lang,
              });
            }

            break;
          }
        }

      },
      onAcceptContact: (contactId) => {
        lilframe.updateContact(contactId, "accepted", true);
      },
      updateContacts: (input, areaName) => {
        if (input.value === "") {
          // reload connected contacts
          lilframe.getCurrentUserContacts((contacts) => {

            self.contacts = contacts;

            lavatube.updateArea(areaName, {
              connectedContacts: contacts,
              lang: self.lang
            });
          });

        } else {
          lilframe.getUsersListBySearch(input.value, (contactsToAdd) => {

            let connectedContactsIds = [];
            for (let contact of self.contacts) {
              connectedContactsIds.push(contact.id);

              // remove from contactsToAdd since its already connected
              for (let i = 0; i < contactsToAdd.length; i++) {
                if (contact.id === contactsToAdd[i].id) {
                  contactsToAdd.splice(i, 1);
                  i--;
                }
              }
            }

            lavatube.updateArea(areaName, {
              contactsToAdd: contactsToAdd,
              connectedContacts: self.contacts,
              connectedContactsIds: connectedContactsIds,
              lang: self.lang
            });
          });
        }
      },
      onClearSearch: (areaName) => {
        // reload connected contacts
        lilframe.getCurrentUserContacts((contacts) => {

          self.contacts = contacts;

          lavatube.updateArea(areaName, {
            connectedContacts: contacts,
            lang: self.lang
          });
        });

      },
      onAddContact: (contactId) => {
        lilframe.addContactById(contactId);
      },

      // every kinda back will result this
      onBack: () => {
        lavatube.renderArea("right-content", "templates/main-empty.html", {
          lang: self.lang
        });
      }
    }); // lavatube.onMethod

  } // constructor

}