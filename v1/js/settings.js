
class Settings {
  lang;
  currentUser;

  // elements
  fileElem;

  constructor(lang, currentUser) {
    let self = this;

    self.lang = lang;
    self.currentUser = currentUser;

    lavatube.renderArea("right-content", "templates/main-settings.html", {
      lang: self.lang,
      currentUser: self.currentUser
    });

    lavatube.onMethod({
      onSelectProfileImage: (input) => {
        self.fileElem = input;
      },
      onUploadProfileImage: () => {
        if (self.fileElem) {

          if (self.fileElem.files !== undefined) {
            let files = self.fileElem.files;

            if (files.length === 1) {
              lilframe.setProfileImage(files[0], (isSuccess) => {

                if (isSuccess) {
                  lilframe.cmd('wrapperNotification', ['done', 'Your profile image has been updated', 5000]);
                } else {
                  // TODO: display error differently not for user like notification
                  //lilframe.cmd('wrapperNotification', ['error', 'Error uploading profile file', 5000]);
                }

              });
            } else  {
              lilframe.cmd('wrapperNotification', ['error', 'Select only 1 file', 5000]);
            }
          }

        }
      },
      onLogout: () => {
        //lavaTubeGlobal.setHref("?do=logout");
        window.location.href = '?do=logout';
      }
    });
  }
}