const lavatube = new LavaTube(debug=true);
const lilframe = new LilFrame(debug=true);


lavatube.onHref((href) => dealWithIt());
dealWithIt();

function dealWithIt() {

  // clear renderer content
  lavatube.clearRenderer();

  // if we have choosed id to chat with
  if (!lavatube.emptyOrUndefined(lavatube.attribute("chat"))) {

    lavatube.render("templates/chat.html");

    return;
  }

  console.log(window.location.href);

  switch (lavatube.attribute("do")) {

    case lavatube.emptyOrUndefined:
    default:

      lilframe.onReady(() => {
        lilframe.isLoggedIn((statement) => {
          if (statement === true) {
            new Chat();
          } else {
            new Login();
          }
        });
      });
      break;

  }

}
