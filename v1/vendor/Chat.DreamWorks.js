
class ChatDreamWorks {
  socket;
  connected = false;

  //SERVER_1_ADDRESS = "wss://chat-dreamworks-s1.herokuapp.com";
  SERVER_1_ADDRESS = "ws://192.168.8.100:8080";


  constructor () {
    let self = this;

    self.socket = new WebSocket(self.SERVER_1_ADDRESS);

    self.socket.onopen = (e) => {
      self.connected = true;
    };

    self.socket.onmessage = (event) => {
      let jsonData = JSON.parse(event.data);
      console.log("received: " + event.data);

      if (jsonData['version'] === '0.1') {

        let header = jsonData['header'];
        let body = jsonData['body'];

        switch (header['action']) {

          case "getPublicKey": {


            lilframe.decryptMessageFromPrivateKey(header['from'], body, (decryptedMessage) => {
              console.log(decryptedMessage);

              body = JSON.parse(decryptedMessage);
              lilframe.updateContact(header['from'], "publicKey", body['myPublicKey']);

            });

            //self.sendPublicKey();
            break;
          }

        }

      }

    }

  }

  tellMyAddress(myAddress) {
    let self = this;

    if (self.connected === false) {
      throw "websocket isn't connected yet"
    }

    // send who has connected

    let jsonData = {
      version: "0.1",
      header: {
        address: myAddress,
        action: "connected"
      }
    };

    let jsonRaw = unescape(encodeURIComponent(JSON.stringify(jsonData, undefined, '\t')));
    self.socket.send(jsonRaw);
  }

  getPublicKey (fromAddress, toAddress, publicKey, callback) {
    let self = this;

    if (self.connected === false) {
      throw "websocket isn't connected yet"
    }

    lilframe.encryptMessageWithPrivateKey(
      unescape(encodeURIComponent(JSON.stringify({ myPublicKey: publicKey }, undefined, '\t'))),
      (encryptedMessage) => {
        let jsonData = {
          version: "0.1",
          header: {
            from: fromAddress,
            to: toAddress,
            action: "getPublicKey"
          },
          body: encryptedMessage
        };

        let jsonRaw = unescape(encodeURIComponent(JSON.stringify(jsonData, undefined, '\t')));
        self.socket.send(jsonRaw);
      }
    );



  }

  sendPublicKey (fromAddress, toAddress) {
    let self = this;

    if (self.connected === false) {
      throw "websocket isn't connected yet"
    }

    lilframe.encryptMessageWithPrivateKey(
      unescape(encodeURIComponent(JSON.stringify({ myPublicKey: "123" }, undefined, '\t'))),
      (encryptedMessage) => {

        let jsonData = {
          version: "0.1",
          header: {
            from: fromAddress,
            to: toAddress,
            action: "publicKey"
          },
          body: encryptedMessage
        };

        let jsonRaw = unescape(encodeURIComponent(JSON.stringify(jsonData, undefined, '\t')));
        self.socket.send(jsonRaw);
      }
    );


  }
}