
class LilFrame extends ZeroFrame {
  debug;
  siteInfo;
  currentUser;

  loginCallback;

  DEFAULT_USER_DESCRIPTION = "Random Dream Chat User";
  DEFAULT_USER_PROFILE_IMAGE = "default/profile.jpg";
  USERS_TXT = "users.txt";

  fileBuffer = {};

  //
  // settings variables
  //

  // BOOLEAN
  DISMISS_USER = "dismissUser";
  // JSON ARRAY
  CONTACTS_ARR = "contacts";
  PROFILE_IMAGE = "profileImage";
  PROFILE_IMAGE_FILE = "profile.image";


  constructor (debug = false) {
    super();
    let self = this;

    self.debug = debug;

    if (debug) {
      console.log("[LilFrame] init(debug="+debug+")")
    }

    // somehow this will get called out plenty
    self.cmd("siteInfo", {}, (res) => {
      if (self.siteInfo === undefined) {
        self.siteInfo = res;

        if (self.debug) {
          console.log("[LilFrame] ready")
        }
      }

    }); // self.cmd("siteInfo"
  }


  //
  // get user 'data.json' location by its id(address)
  //

  getDataJsonByUserAddress (userAddress) {
    return "data/users/"+userAddress+"/data.json";
  }


  //
  // get user 'content.json' location by its id(address)
  //

  getContentJsonByUserAddress (userAddress) {
    return "data/users/"+userAddress+"/content.json";
  }


  //
  // get user 'content.json' location by its id(address)
  //

  getContactsJsonByUserAddress (userAddress) {
    return "data/users/"+userAddress+"/ignore/contacts.json";
  }


  //
  // get current user optional file path
  //

  getOptionalPathByUserAddress (userAddress) {
    return "data/users/"+userAddress+"/optional";
  }


  //
  // get use private key file path
  //

  getPrivateKeyPathByUserAddress (userAddress) {
    return "data/users/"+userAddress+"/ignore/privateKey.bin";
  }



  //
  // if site is ready to handle zeronet specific requests
  //

  onReady (callback) {
    let self = this;

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    let calledBackAlready = false;
    let checkSiteInfoInterval = setInterval(() => {
      if (self.siteInfo !== undefined && self.siteInfo != null) {
        if (calledBackAlready === false) {
          calledBackAlready = false;
          clearInterval(checkSiteInfoInterval);
          callback();
        }
      }
    }, 100);

  } // onReady


  //
  // callback will fire if user selects certificate to use
  //

  onLogin (callback) {
    let self = this;

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    self.loginCallback = callback;

  } // onLogin


  //
  // on open websocket
  //

  onOpenWebsocket () {
    // sucessfully connected
  }

  //
  // on request
  //

  onRequest (cmd, message) {
    let self = this;

    if (cmd === "setSiteInfo") {

      // if login is success
      if (message.params.auth_address && message.params.cert_user_id) {

        // login is success

        self.getSettings((settings) => {
          settings[self.DISMISS_USER] = false;

          self.setSettings(settings, () => {
            if (self.loginCallback !== undefined && self.loginCallback != null) {
              self.loginCallback();
            }
          }); // self.setSettings

        }); // self.getSettings

      } // if (message.params.auth_address && message.params.cert_user_id)

      // Save site info data to allow access it later
      self.siteInfo = message.params;
    }

  } // onRequest



  //
  // select user to log in with
  //

  selectUser () {
    let self = this;

    self.cmd("certSelect", {accepted_domains: ["zeroid.bit"]});
  }



  //
  // get current user
  //

  getCurrentUser (callback) {
    let self = this;
    let user = {};

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    let dataInnerPath = self.getDataJsonByUserAddress(self.siteInfo.auth_address);

    if (self.currentUser == null) {
      if (self.siteInfo.auth_address && self.siteInfo.cert_user_id) {
        self.cmd("fileGet", {"inner_path": dataInnerPath, "required": false}, (data) => {
          if (data) {
            user = JSON.parse(data);
            console.log(user);

            user["id"] = self.siteInfo.auth_address;
            user["name"] = self.siteInfo.cert_user_id.split("@")[0];

            let keysNeeded = {
              description: self.DEFAULT_USER_DESCRIPTION,
              profileImage: self.DEFAULT_USER_PROFILE_IMAGE,
              publicKey: ""
            };

            let allExists = true;

            for (let key of Object.keys(keysNeeded)) {
              if (user[key] === undefined || user[key] === "") {
                allExists = false;
              }
            }

            if (!allExists) {
              // will sign user data
              self.checkCurrentUserData(user, keysNeeded, (user) => {
                self.currentUser = user;
                callback(user);
              });
            } else {
              self.currentUser = user;
              callback(user);
            }

          } // if (data)
          else {
            // user does not exists in our system yet
            // then just create empty user and recall this function to later fill missings
            self.editCurrentUserData(null, null, false, (user) => {
              self.getCurrentUser(callback);
            }); // self.saveUserData("description"
          }
        }); // self.cmd("fileGet"
      } // if (self.siteInfo && self.siteInfo.auth_address && self.siteInfo.cert_user_id)
      else {
        callback(null)
      }
    } // if (self.currentUser === undefined)
    else {
      callback(self.currentUser);
    }

  } // getCurrentUser


  //
  // check for attributes and add default if it doesnt exists
  //

  checkCurrentUserData (user, data, callback = null) {
    let self = this;

    if (user === undefined || user == null) {
      throw "user(1st arg) is not set";
    }

    if (data === undefined || data == null) {
      throw "data(2nd arg) is not set";
    }


    let allExists = true;

    for (let key of Object.keys(data)) {
      if (user[key] === undefined) {

        console.log(key + " is undefined");
        self.editCurrentUserData(key, data[key], false, (isSuccess) => {
          // TODO: error check if not success
          user[key] = data[key];
          self.checkCurrentUserData(user, data, callback);
        });

        allExists = false;
        break;

      }
    }

    if (allExists) {
      // also check for pgp key pairs
      if (user['publicKey'] === undefined || user['publicKey'] === "") {
          // this will also sign user data.json
        self.createUserKeyPairs(user, callback);
      } else {
        self.editCurrentUserData(null, null, true, (isSuccess) => {
          // TODO: error check if not success
          if (callback != null) {
            callback(user);
          }
        });
      }
    } // if (allExists)

  } // checkCurrentUserData



  //
  // create pgp key pairs for user
  //

  createUserKeyPairs (user, callback = null) {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    var options = {
      userIds: [{ name: user["name"], id: user["id"] }],
      curve: "ed25519",
      passphrase: self.siteInfo.auth_key
    };

    openpgp.generateKey(options).then(function(key) {
      var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
      var pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '

      let userPrivateKeyInnerPath = self.getPrivateKeyPathByUserAddress(self.siteInfo.auth_address);

      self.cmd("fileWrite", [userPrivateKeyInnerPath, btoa(privkey)], (res) => {
        if (res === "ok") {

        } // if (res === "ok")
        else {
          console.error("[LilFrame] ERROR: checkCurrentUserData - fileWrite: failed");
        }
      });

      self.editCurrentUserData("publicKey", pubkey, true, (isSuccess) => {
        user["publicKey"] = pubkey;

        // TODO: error check if not success
        if (callback != null) {
          callback(user);
        }
      });
    });//  openpgp.generateKey

  }


  //
  // edit user data
  //

  editCurrentUserData (key, value, publish = true, callback = null) {
    let self = this;
    let user = {};

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let dataInnerPath = self.getDataJsonByUserAddress(self.siteInfo.auth_address);

    self.cmd("fileGet", {"inner_path": dataInnerPath, "required": false}, (data) => {
      if (data) {
        user = JSON.parse(data);
      }

      if (key != null) {
        user[key] = value;
      }

      let jsonRaw = unescape(encodeURIComponent(JSON.stringify(user, undefined, '\t')));

      // Write file to disk
      self.cmd("fileWrite", [dataInnerPath, btoa(jsonRaw)], (res) => {
        if (res === "ok") {
          if (publish) {
            self.currentUserSignContent(() => {
              if (callback != null) {
                callback(true);
              } // if (callback != null)
            }); // self.userSign
          } // if (publish)
          else {
            if (callback != null) {
              callback(true);
            } // if (callback != null)
          }
        } // if (res === "ok")
        else {
          self.cmd("wrapperNotification", ["error", "File write error: #{res}"]);

          if (callback != null) {
            callback(false);
          } // if (callback != null)
        }
      }); // self.cmd("fileWrite"
    }); // self.cmd("fileGet"

  } // saveCurrentUserData


  //
  // save current user content.json
  //

  currentUserSignContent (callback = null) {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let contentInnerPath = self.getContentJsonByUserAddress(self.siteInfo.auth_address);

    // sign the changed file in our user's directory
    self.cmd("siteSign", {"inner_path": contentInnerPath}, (res) => {

      // clear current user from buffer to read it again
      self.currentUser = null;

      // publish to other users
      self.cmd("sitePublish", {"inner_path": contentInnerPath, "sign": false}, function(res) {

      }); // self.cmd("sitePublish"

      if (callback != null) {
        callback();
      } // if (callback != null)
    }); // self.cmd("siteSign"

  } // currentUserSignContent


  //
  // fix current user content by editing it manually
  // returns boolean if function was success
  //

  fixCurrentUserContent (callback) {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    let contentInnerPath = self.getContentJsonByUserAddress(self.siteInfo.auth_address);

    self.cmd("fileGet", {"inner_path": contentInnerPath, "required": false}, (data) => {

      if (data) {

        let content = JSON.parse(data);
        let changed = false;

        if (content["optional"] === undefined) {
          content["optional"] = "(upload/.*)";
          changed = true;
        }

        if (content["ignore"] === undefined) {
          content["ignore"] = "(ignore/.*)";
          changed = true;
        }

        if (changed) {
          let json_raw = unescape(encodeURIComponent(JSON.stringify(content)));
          self.cmd("fileWrite", [contentInnerPath, btoa(json_raw)], (res) => {
            if (res === "ok") {

            } else {
              console.error("[LilFrame] ERROR: fixUserContent - fileWrite: failed");
            }

            callback(true);
          });
        } // if (changed)
        else {
          callback(true);
        }
      } // if (data)
      else {
        callback(false);
      }
    }); // self.cmd("fileGet"

  } // fixCurrentUserContent



  //
  // get language file as json object
  //

  getLanguageFileAsJsonObject (filePath, callback) {
    let self = this;

    if (filePath === undefined || filePath === "") {
      throw "filePath can't be undefined or empty";
    }

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    self.cmd("fileGet", {"inner_path": filePath, "required": false}, (data) => {
      if (data) {
        callback(JSON.parse(data));
      } else {
        console.error("[LilFrame] ERROR: getLanguageFileAsJsonObject - fileGet file doesnt exists: " + filePath);
        callback([]);
      }
    }); // self.cmd("fileGet"
  }



  //
  // get settings
  //

  getSettings (callback) {
    let self = this;
    let returned = false;

    self.cmd("userGetSettings", [], (res) => {
      // dunny why but this 'userGetSettings' causes many times to respond
      if (returned === false) {
        returned = true;
        callback(res);
      }
    }); // self.cmd("userGetSettings"

  } // getSettings


  //
  // save/set site settings
  //

  setSettings (settings, callback = null) {
    let self = this;

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    self.cmd("userSetSettings", [settings], (res) => {
      if (callback != null) {
        callback(res);
      }
    }); // self.cmd("userSetSettings"

  } // setSettings


  //
  // log out current user, zero out selected certificate
  //

  logoutCurrentUser (callback = null) {
    let self = this;

    self.getSettings((settings) => {
      settings[self.DISMISS_USER] = true;
      self.setSettings(settings, callback);
    }); // self.getSettings

  } // logoutCurrentUser


  //
  // is current user logged in
  //

  isLoggedIn (callback) {
    let self = this;

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    self.getCurrentUser((user) => {
      if (user == null) {
        callback(false);
      } else {
        self.getSettings((settings) => {
          if (settings[self.DISMISS_USER] === true) {
            callback(false);
          } else {
            // fix user content.json
            self.fixCurrentUserContent(() => {
              callback(true);
            }); // self.fixCurrentUserContent
          }
        }); // self.getSettings
      }
    }); // self.getCurrentUser
  }


  //
  // set(upload) profile image
  //

  setProfileImage (file, callback = null) {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let optionalInnerPath = self.getOptionalPathByUserAddress(self.siteInfo.auth_address);

    self.cmd("bigfileUploadInit", [optionalInnerPath + "/" + self.PROFILE_IMAGE_FILE, file.size], (initRes) => {
      let formData = new FormData();
      formData.append(file.name, file);

      let req = new XMLHttpRequest();
      req.upload.addEventListener("progress", console.log);
      req.upload.addEventListener("loadend", () => {
        self.editCurrentUserData(self.PROFILE_IMAGE, initRes.inner_path, true, (isSuccess) => {
          if (callback != null) {
            callback(isSuccess);
          }
        });
      });
      req.withCredentials = true;
      req.open("POST", initRes.url);
      req.send(formData)
    })
  }


  //
  // get name and user id/address close to search result
  // will return array of search results, even empty arr if file fails to open
  //

  getUsersListBySearch (searchX, callback) {
    let self = this;

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    self.cmd("fileGet", {"inner_path": self.USERS_TXT, "required": false}, (data) => {
      if (data) {
        let lines = data.split("\n");
        let ret = [];

        for (let i = 0; i < lines.length; i += 2) {
          if (lines[i].indexOf(searchX) !== -1 || searchX.indexOf(lines[i]) !== -1) {

            ret.push({name: lines[i], id: lines[i +1]});
          }
        }

        let got = 0;
        for (let i = 0; i < ret.length; i++) {

          setTimeout((j) => {

            let dataInnerPath = self.getDataJsonByUserAddress(ret[j].id);
            self.getFileWithBuffer(dataInnerPath, (dataInner) => {

              let jo = JSON.parse(dataInner);
              ret[j][self.PROFILE_IMAGE] = jo[self.PROFILE_IMAGE] === undefined ? self.DEFAULT_USER_PROFILE_IMAGE : jo[self.PROFILE_IMAGE];
              got += 1;

              // if we have cathered every thing
              if (got === ret.length) {
                callback(ret);
              }

            });

          }, 0, i);

        }

      } else {
        console.error("[LilFrame] ERROR: getUsersListBySearch - fileGet file doesnt exists: " + self.USERS_TXT);
        callback([]);
      }
    });

  } // parseUsersList


  //
  // get current user connected users
  //

  getCurrentUserContacts (callback) {
    let self = this;

    if (callback === undefined || callback == null) {
      throw "callback has to be defined";
    }

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let contactsInnerPath = self.getContactsJsonByUserAddress(self.siteInfo.auth_address);

    self.cmd("fileGet", {"inner_path": contactsInnerPath, "required": false}, (data) => {
      if (data) {
        let jsonData = JSON.parse(data);


        let got = 0;
        for (let i = 0; i < jsonData[self.CONTACTS_ARR].length; i++) {

          setTimeout((j) => {

            let dataInnerPath = self.getDataJsonByUserAddress(jsonData[self.CONTACTS_ARR][j].id);
            self.getFileWithBuffer(dataInnerPath, (dataInner) => {

              let jo = JSON.parse(dataInner);
              jsonData[self.CONTACTS_ARR][j][self.PROFILE_IMAGE] = jo[self.PROFILE_IMAGE] === undefined ? self.DEFAULT_USER_PROFILE_IMAGE : jo[self.PROFILE_IMAGE];
              got += 1;

              // if we have cathered every thing
              if (got === jsonData[self.CONTACTS_ARR].length) {
                callback(jsonData[self.CONTACTS_ARR]);

              }

            });

          }, 0, i);

        }

      } else {
        console.warn("[LilFrame] WARNING: getCurrentUserContacts - fileGet file doesnt exists: " + contactsInnerPath);
        callback([]);
      }
    }); // self.cmd("fileGet"

  } // getCurrentUserContacts


  //
  // add contact to connected ones, but not fully, next step is to ask chat key to fully accept contact
  //

  addContactById (contactId, callback = null, publicKey = "") {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let contactsInnerPath = self.getContactsJsonByUserAddress(self.siteInfo.auth_address);

    self.cmd("fileGet", {"inner_path": contactsInnerPath, "required": false}, (data) => {
      let jsonData = {};

      if (data) {
        jsonData = JSON.parse(data);
      }

      if (jsonData[self.CONTACTS_ARR] === undefined) {
        jsonData[self.CONTACTS_ARR] = [];
      }

      let contentInnerPath = self.getContentJsonByUserAddress(contactId);
      self.cmd("fileGet", {"inner_path": contentInnerPath, "required": false}, (contentData) => {
        if (contentData) {
          let name = JSON.parse(contentData)['cert_user_id'].split("@")[0];

          jsonData[self.CONTACTS_ARR].push({
            id: contactId,
            name: name,
            publicKey: publicKey,
            accepted: false
          });

          let jsonRaw = unescape(encodeURIComponent(JSON.stringify(jsonData, undefined, '\t')));

          // Write file to disk
          self.cmd("fileWrite", [contactsInnerPath, btoa(jsonRaw)], (res) => {
            if (res === "ok") {
              console.log("[LilFrame] LOG: addContactById - fileWrite was success: " + contactsInnerPath);
            } else {
              console.error("[LilFrame] ERROR: addContactById - fileWrite: " + contactsInnerPath);
            }
          });
        } else {
          console.error("[LilFrame] ERROR: addContactById - fileGet file doesnt exists: " + contentInnerPath);
        }

      }); // self.cmd("fileGet"

    }); // self.cmd("fileGet"
  }


  //
  // just add public key to contact
  //

  updateContact (contactId, key, value, callback = null) {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let contactsInnerPath = self.getContactsJsonByUserAddress(self.siteInfo.auth_address);

    self.cmd("fileGet", {"inner_path": contactsInnerPath, "required": false}, (data) => {
      let jsonData = {};

      if (data) {
        jsonData = JSON.parse(data);
      }

      if (jsonData[self.CONTACTS_ARR] === undefined) {
        if (key === "publicKey") {
          self.addContactById(contactId, callback, value);
        } else {
          console.error("[LilFrame] ERROR: updateContactPublicKey - cannot set key '" + key + "' cause u r missing this contact");
        }
      } else {
        for (let e of jsonData[self.CONTACTS_ARR]) {
          if (e['id'] === contactId) {
            e[key] = value;
            break;
          }
        }

        let jsonRaw = unescape(encodeURIComponent(JSON.stringify(jsonData, undefined, '\t')));

        self.cmd("fileWrite", [contactsInnerPath, btoa(jsonRaw)], (res) => {
          if (res === "ok") {
            console.log("[LilFrame] LOG: addContactById - fileWrite was success: " + contactsInnerPath);
          } else {
            console.error("[LilFrame] ERROR: addContactById - fileWrite: " + contactsInnerPath);
          }
        });
      }
    });
  }


  //
  // get file with buffer
  //

  // TODO: clear buffer if site has updated
  getFileWithBuffer (filePath, callback) {
    let self = this;

    if (self.fileBuffer[filePath] !== undefined) {
      callback(self.fileBuffer[filePath]);
    } else {

      self.cmd("fileGet", {"inner_path": filePath, "required": false}, (data) => {
        self.fileBuffer[filePath] = data;
        callback(data);
      });

    }
  }



  //
  // encrypt message with current user private key
  //

  encryptMessageWithPrivateKey (message, callback) {
    let self = this;

    if (self.siteInfo === undefined || self.siteInfo == null) {
      throw "siteInfo is not set";
    }

    let privateKeyInnerPath = self.getPrivateKeyPathByUserAddress(self.siteInfo.auth_address);

    self.cmd("fileGet", {"inner_path": privateKeyInnerPath, "required": true}, (privateKey) => {
      openpgp.key.readArmored(privateKey).then(armored => {
        if (armored.keys.length === 0) {
          console.error("[LilFrame] ERROR: encryptMessageById - error occur while encrypting");
        } else {

          var privKeyObj = armored.keys[0];
          privKeyObj.decrypt(self.siteInfo.auth_key).then(function() {
            var options = {
              message: openpgp.message.fromText(message),
              privateKeys: [privKeyObj]
            };

            openpgp.sign(options).then(ciphertext => {
              callback(ciphertext.data);
            }); // openpgp.sign
          });
        }
      }); // openpgp.key.readArmored
    });
  }


  //
  // decrypt message thats encrypted with private key
  //

  decryptMessageFromPrivateKey (userId, message, callback) {
    var self = this;

    let privateKeyInnerPath = self.getDataJsonByUserAddress(userId);
    self.cmd("fileGet", {"inner_path": privateKeyInnerPath, "required": true}, (data) => {
      let jsonData = JSON.parse(data);

      if (jsonData['publicKey'] === undefined || jsonData['publicKey'] === "") {
        console.error("[LilFrame] ERROR: decryptMessageFromPrivateKey - user '" + userId + "' missing public key");
        return;
      }

      openpgp.key.readArmored(jsonData["publicKey"]).then(armored => {
        if (armored.keys.length === 0) {
          console.error("[LilFrame] ERROR: decryptMessageFromPrivateKey - user '" + userId + "' has incorrect public key");
        } else {
          openpgp.message.readArmored(message).then(msg => {
            var options = {
              message: msg,
              publicKeys: armored.keys
            };

            openpgp.verify(options).then(plaintext => {
              callback(new TextDecoder("utf-8").decode(plaintext.data));
            }); // openpgp.verify
          });
        }
      }); // openpgp.key.readArmored(localUser["publicKey"]

    });

  }

}